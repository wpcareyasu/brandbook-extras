jQuery(document).ready(function($) {
  /*
   *  Anchor Sidebar Menu
   */
  //Get all anchor links in the sidebar menu.
  $anchorMenu = $('.sidebar .menu .sub-menu .menu-item');
  //Get all anchor links in the content region.
  $anchorLinks = $('.wrapper a.anchor');

  //Extend menu functionality to treat anchor menu links in the sidebar menu as if they were pages.
  $anchorMenu.on('click', function(event) {
    $(this).parent().find('li').removeClass('current_page_item');
    $(this).addClass('current_page_item');
  });

  //On window scroll check anchor links in the content region
  $(window).on('scroll', function() {
    var windowTop = $(window).scrollTop();
    $anchorLinks.each(function() {
      //If the anchor link position is above half the height of the page, select that as the active element in the sidebar menu.
      if (windowTop >= ($(this).offset().top - ($(window).height() / 2)) && windowTop !== 0) {
        var id = $(this).attr('name');
        $anchorMenu.removeClass('current_page_item');
        $anchorMenu.siblings().find('a[href=#' + id + ']').parent().addClass('current_page_item');
      } else if (windowTop === 0) {
        //If page is at the top, then remove the activated anchor link.
        $anchorMenu.removeClass('current_page_item');
      }
    });
  });

  /*
   *  Affix Sidebar Menu
   */
  if ($('.sidebar .widget_nav_menu').length > 0 || $('.sidebar .widget_advanced_menu').length > 0) {

    var affixItem = function(itemHeight, windowHeight, footerHeight) {
      if (itemHeight < windowHeight) {
        $('body.page .sidebar').affix({
          offset: {
            top: $('main').offset().top,
            bottom: footerHeight
          }
        }).on('affix.bs.affix', function() { // before affix
          $(this).css({
            //'width': $(this).parent().innerWidth()  // variable widths
          });
        }).on('affix-bottom.bs.affix', function() { // before affix-bottom
          $(this).css('bottom', 'auto'); // THIS is what makes the jumping
        });
      }
    };

    //Affix item if it's clicked on.
    var sidebarHeight = $('body.page .sidebar').height() + 65;
    var windowHeight = $(window).height();
    var footerHeight = $('footer').innerHeight() + $('.asu-footer').innerHeight();

    affixItem(sidebarHeight, windowHeight, footerHeight);

    $('.sidebar .widget_nav_menu .menu-item.menu-item-has-children .sub-items > a').click(function() {
      if ($(this).attr('href') === '#') {
        $(this).parent().toggleClass('current-menu-parent');
        affixItem(sidebarHeight, windowHeight, footerHeight);
      }
    });

  }
});
