<?php
/*
Plugin Name: Brandbook Extras
Plugin URI: http://wordpress.org/plugins/menu-item1/
Description: Extra functionality for the Brandbook
Author: Danny Martinez
Version: 0.2.2
Author URI: http://martinezd.com
Bitbucket Plugin URI: https://bitbucket.org/wpcareyasu/brandbook-extras
Bitbucket Branch: master
*/

function brandbook_extras_enqueue_style() {
  wp_enqueue_style( 'brandbook_extras_style', plugins_url( 'extras.css', __FILE__ ) );
  wp_enqueue_script( 'brandbook_extras_script', plugins_url( 'extras.js', __FILE__ ), array('jquery'), false, true );
}

add_action( 'wp_enqueue_scripts', 'brandbook_extras_enqueue_style' );

//add pagination links at the bottom of ever page
add_filter('the_content', 'add_my_content');
function add_my_content($content) {
  if ( get_post_type() == 'page' ){
    global $post;
    $pagelist = get_pages('sort_column=menu_order&amp;sort_order=asc');
    $pages = array();
    foreach ($pagelist as $page) {
      $pages[] += $page->ID;
    }
  
    $current = array_search($post->ID, $pages);
    $prevID = (isset($pages[$current-1])? $pages[$current-1] : '');
    $nextID = (isset($pages[$current+1])? $pages[$current+1] : '');
  
    ob_start();
    ?>
    <div class="page-navigation">
      <?php if (!empty($prevID)) { ?>
      <div class="previous"><a href="<?php echo get_permalink($prevID); ?>"><h4><span class="small">Previous</span><br><?php echo   get_the_title($prevID); ?></h4></a></div>
      <?php }
    if (!empty($nextID)) { ?>
      <div class="next"><a href="<?php echo get_permalink($nextID); ?>"><h4><span class="small">Next</span><br><?php echo   get_the_title($nextID); ?></h4></a></div>
    <?php } ?>
    </div><!-- .navigation -->
    <?php
    $output = ( (is_front_page())? '' : ob_get_contents() );
    ob_end_clean();
    $content = $content . $output;
  }
  return $content;

}